# Start using Git on the command-line

To start using GitLab using the command-line, you will need the following:
- [An account](https://gitlab.com/users/sign_in) on GitLab.
- A shell to access Git's functionality using the command-line.
- The Git command-line program; either as an individual program or as a part of the shell itself.

## Open a shell

Fire up the shell of your preference.

Below are some of the popular options on OS X, Windows and Linux.

- [Terminal](http://blog.teamtreehouse.com/introduction-to-the-mac-os-x-command-line) on  OS X

- [GitBash](https://msysgit.github.io) on Windows

- [Linux Terminal](http://www.howtogeek.com/140679/beginner-geek-how-to-start-using-the-linux-terminal/) on Linux

## Check if Git has already been installed

Git is often pre-installed on OS X and Linux.

To check if that is the case, type the following command into the shell and then press enter:
```
git --version
```
If Git is installed, you should see a message that will tell you which Git version you have in your computer.

![git --version command](basicsimages/git_version_cli.png)

If you don’t receive a "Git version" message, it means that you need to [install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) on your operating system.

You can also install Git using one of the [official installers](https://git-scm.com/downloads).

Once you have finished installing Git, open a new shell and type "git --version" once again to verify that it was correctly installed.

## Add your Git username and set your email

Every Git commit that you create will show your username and email. Therefore, it is important to set this properly.

In your shell, type the following command to add your username:
```
git config --global user.name ADD YOUR USERNAME
```

Then verify that you have the correct username:
```
git config --global user.name
```

To set your email address, type the following command:
```
git config --global user.email ADD YOUR EMAIL
```

To verify that you entered your email correctly, type:
```
git config --global user.email
```
Using the "--global" option means that you need to set this information only once. After this, Git will always use this information everywhere.

If you want to override this with a different username or email address for specific projects, you can run the command without the "--global" option when you’re in that project.

## Check your information

To view the information that you entered, type:
```
git config --global --list
```
## Basic Git commands

Let us go through some of the basic Git commands.

### Go to the master branch to pull the latest changes from there

```
git checkout master
```

### Download the latest changes in the project
This is for you to work on an up-to-date copy (it is important to do this every time you work on a project), while you setup tracking branches.
```
git pull REMOTE NAME-OF-BRANCH -u
```
A good example for REMOTE could be 'origin' and the NAME-OF-BRANCH could be 'master' or an existing branch.

### Create a branch
Spaces are not valid in naming branches. You can to use a hyphens or underscores as separators in their place.
```
git checkout -b NAME-OF-BRANCH
```

### Work on a branch that has already been created
```
git checkout NAME-OF-BRANCH
```

### View the changes you have made
Before you start the process to commit your changes, it is important to review the changes you have made.
```
git status
```
### Add changes to commit
Git will highlight all untracked changes in red when you type "git status".

To add any of the files shown in red to the commit, you have to use the following command:
```
git add FILENAME
```

Once you have added the files, commit them with a message that describes the intention of the commit.

This will help others understand easily what is the purpose of the commit.
```
git commit -m "DESCRIBE THE INTENTION OF THE COMMIT"
```

### Send changes to gitlab.com
```
git push REMOTE NAME-OF-BRANCH
```

### Delete all changes in the Git repository, but leave unstaged things
```
git checkout .
```

### Delete all changes in the Git repository, including untracked files
```
git clean -f
```

### Merge created branch with master branch
You need to be in the created branch to successfully complete this operation.
```
git checkout NAME-OF-BRANCH
git merge master
```
